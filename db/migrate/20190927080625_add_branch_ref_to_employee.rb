class AddBranchRefToEmployee < ActiveRecord::Migration[5.2]
  def change
    add_reference :employees, :branch, foreign_key: true
  end
end
