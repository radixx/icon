class AddEmployeeRefToLaptop < ActiveRecord::Migration[5.2]
  def change
    add_reference :laptops, :employee, foreign_key: true
  end
end
