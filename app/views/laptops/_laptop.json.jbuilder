json.extract! laptop, :id, :brand, :type, :is_active, :created_at, :updated_at
json.url laptop_url(laptop, format: :json)
